package dev.rehm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalculatorTest {

    // input: 4, 6
    // output: 5
    @Test
    public void testAvgOfTwoNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(4,6);
        double expected = 5;
        assertEquals(expected, actual);
    }

    @Test
    public void testAvgOfAPositiveAndNegativeValue(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(-4,4);
        double expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void testAvgOfThreeNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(5,10,15);
        double expected = 10;
        assertEquals(expected,actual);
    }

    @Test
    public void testAvgOfFourNumbers(){
        Calculator calculator = new Calculator();
        double actual = calculator.findAverage(5,10,15,14);
        double expected = 11;
        assertEquals(expected,actual);
    }

    @Test
    public void testEmptyArray(){
        Calculator calculator = new Calculator();
        // executing the findAverage method with no parameters throws an IllegalArgumentException
        assertThrows(IllegalArgumentException.class, ()->{
            calculator.findAverage();
        });
    }

//    fizzBuzz
//
//
//    Write a “fizzBuzz” method that accepts a number as input and returns it as a String.
//    For multiples of three return “Fizz” instead of the number
//    For the multiples of five return “Buzz”
//    For numbers that are multiples of both three and five return “FizzBuzz”.
    @Test
    public void testfizzBuzz(){
        Calculator calculator = new Calculator();
        String actual = calculator.fizzBuzz(15);
        String expected = "FizzBuzz";
        assertEquals(expected, actual);
    }

//    findMax
//
//
//    Write a method that takes in an int array, and returns the max value in an array. This can be included
//    Your method should be able to accommodate an array that contains only negative numbers
//    Challenge Q: if the user attempts to use the FindMax method with a null input or an empty array, a custom exception should be thrown
    @Test
    public void testFindMax(){
        Calculator calculator = new Calculator();
        int[] arr = {-2, -5, -1, -9, -4, -7};
        assertEquals(-1, calculator.findMax(arr));
    }
}
