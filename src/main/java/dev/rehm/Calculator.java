package dev.rehm;

public class Calculator {
    /*
    public double findAverage(double num1, double num2){
        return (num1+num2)/2;
    }

    public double findAverage(double num1, double num2, double num3){
        return (num1+num2+num3)/3;
    }
     */

    public double findAverage(double... nums){ // [] -> length = 0
        if(nums.length==0){
            throw new IllegalArgumentException("cannot invoke method with no numbers");
        }
        double sum = 0;
        for(double num: nums){
            sum += num;
        }
        return sum/nums.length;
    }

    public String fizzBuzz(int num){
        if(num % 3 != 0 && num % 5 != 0){
            return null;
        }
        else if(num % 3 == 0 && num % 5 == 0){
            return "FizzBuzz";
        }else if(num % 3 == 0){
            return "Fizz";
        } else if(num % 5 == 0){
            return "Buzz";
        }
       return null;
    }

    public int findMax(int[] arr){
        if(arr == null){throw new IllegalArgumentException("");}
        int max = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(arr[i] > max){
                max = arr[i];
            }
        }
        return max;
    }

}
